import json
from logging import log
import requests
from requests.auth import HTTPBasicAuth
from loguru import logger
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Enviroments
ClientID = 'f2c75c7b48ab209ace2fb0d83cc287a530803c97031e684eb5ffa871a4b0c0c0'
ClientSecret = '1ac4f0125170a68b98b3f7020604a94212cd0d1f2a8011fa5f566501eb52e8aa'
BasicAuth = 'cmFxdWVsLnNhbmNoZXpAYWxpZ25ldC5jb206RVJxYkxQMllHZmJqM0l0T2JxVWMxM0Mw'
server_bizagi = 'https://test-bizagi.alignetsac.com'

# Constantes
CONTENT_TYPE = 'application/json'
XPATH_CODE_PARTICIPANT = 'ListadeParticipantes.sCodigoParticipante'
XPATH_CODE_PROJECT = 'ListadeProyectos.sIddeproyecto'
NEXT_LINK = '@odata.nextLink'

def get_prepared_body(action, msg, success, data=[], errors={}, code='01'):
    payload = {
        "action": action,
        "success": success,
        "data": data,
        "errors": errors,
        "meta": {
            "status": {
                "code": code,
                "message_ilgn": [{
                    "locale": "es_PE",
                    "value": msg
                }]
            }
        }
    }
    return payload


def get_prepared_response(status_code, prepared_body):
    response = {
        'statusCode': status_code,
        'headers': {
            'Content-Type': CONTENT_TYPE,
            'Access-Control-Allow-Origin': '*'
        },
        'body': json.dumps(prepared_body)
    }
    return response


def get_bizagi_token():
    # obtener token
    url_token = f'{server_bizagi}/Iniciativa/oauth2/server/token'
    logger.info("URL_GET_TOKEN - {}", url_token)
    response_token = requests.post(url_token, data='grant_type=client_credentials&scope=api', headers={'Content-Type': 'application/x-www-form-urlencoded'}, auth=HTTPBasicAuth(ClientID, ClientSecret))
    response_status_token = response_token.status_code
    logger.info("RESPONSE_STATUS_CODE_TOKEN - {}", response_status_token)
    response_json_token = response_token.json()

    if response_status_token != 200:
        prepared_body_token = get_prepared_body(
            'post_project_participants', 'Error al obtener token de sesión', False)
        logger.info("RESPONSE_END_TOKEN - {}",
                    get_prepared_response(400, prepared_body_token))
        return get_prepared_response(400, prepared_body_token)

    return get_prepared_response(200, response_json_token)


def get_list_projects_bizagi(bizagi_token):
    size = 50
    page = 1
    repeat = True
    list_bizagi_projects = []
    while repeat:
        # ListadeProyectos(9978487f-8368-4ace-98a5-55516e9df27c): Nombre de la entidad donde se guardan datos del los projectos de Jira
        url_list_projects_bizagi = f'{server_bizagi}/Iniciativa/odata/data/entities(9978487f-8368-4ace-98a5-55516e9df27c)/values'
        skip = (page*size) - size
        param = {'$top': size, '$skip': skip}
        response_bizagi_projects = requests.get(url_list_projects_bizagi, params=param, headers={'Content-type': CONTENT_TYPE, 'Authorization': f'Bearer {bizagi_token}'})
        response_json_list_projects = response_bizagi_projects.json()
        list_projec = response_json_list_projects['value']
        if NEXT_LINK not in response_json_list_projects:
            repeat = False

        for x in list_projec:
            parameters = x['parameters']
            projects_bizagi = {
                'id_bizagi': x['id'],
                'id_jira': None,
                'key_jira': None,
                'name_jira': None
            }
            for i in parameters:
                try:
                    if i['xpath'] == 'sIddeproyecto':
                        projects_bizagi['id_jira'] = i['value']
                except Exception as e:
                    logger.info("No se encontro xpath sIddeproyecto - {}", e)
                try:
                    if i['xpath'] == 'sCodigodeproyecto':
                        projects_bizagi['key_jira'] = i['value']
                except Exception as e:
                    logger.info(
                        "No se encontro xpath sCodigodeproyecto - {}", e)
                try:
                    if i['xpath'] == 'sNombredeproyecto':
                        projects_bizagi['name_jira'] = i['value']
                except Exception as e:
                    logger.info(
                        "No se encontro xpath sNombredeproyecto - {}", e)
            list_bizagi_projects.append(projects_bizagi)
        page += 1

    return list_bizagi_projects


def get_iduser_participants(accound_id, token):
    code_participant = None
    size = 50
    page = 1
    repeat = True
    while repeat:
        # ListadeProyectos(9978487f-8368-4ace-98a5-55516e9df27c): Nombre de la entidad donde se guardan datos del los projectos de Jira
        url_idparticipant = f'{server_bizagi}/Iniciativa/odata/data/entities(68007f3b-7158-4c42-bab0-3b9897b00526)/values'
        skip = (page*size) - size
        payload = {'$top': size, '$skip': skip}
        header = {'Content-Type': CONTENT_TYPE,'Authorization': f'Bearer {token}'}
        response_bizagi_participants = requests.get(url_idparticipant, params=payload, headers=header)
        json_list_participants = response_bizagi_participants.json()

        if NEXT_LINK not in json_list_participants:
            repeat = False

        for x in json_list_participants['value']:
            parameters = x['parameters']
            try:
                for i in parameters:
                    if (i['xpath'] == 'sCodigoParticipante') and (i['value'] == accound_id):
                        code_participant = x['id']
                        return code_participant
            except Exception as e:
                logger.info("No se encontro sCodigoParticipante - {}", e)
        page += 1

    return code_participant


def disable_participants_by_project(id_projectsparticipant, token):
    dsbl = False
    url_get_projectparticipant = f"{server_bizagi}/Iniciativa/odata/data/entities(034eea50-607c-4289-b2e4-5933e24a9ff0)/values({id_projectsparticipant})/update"
    payload = json.dumps({
        "startParameters": [
            {
                "xpath": "dsbl",
                "value": "true"
            }
        ]
    })
    headers = {'Content-Type': CONTENT_TYPE,'Authorization': f'Bearer {token}'}
    response = requests.request("POST", url_get_projectparticipant, headers=headers, data=payload)
    response_status = response.status_code
    if response_status == 200:
        return True

    return dsbl


def get_id_projectsparticipant_by_accoundid_and_projectid(token, accound_id, id_project_jira):
    id_projectsparticipant = None
    size = 50
    page = 1
    repeat = True
    while repeat:
        # ListadeProyectos(9978487f-8368-4ace-98a5-55516e9df27c): Nombre de la entidad donde se guardan datos del los projectos de Jira
        url_get_projectsparticipant = f'{server_bizagi}/Iniciativa/odata/data/entities(034eea50-607c-4289-b2e4-5933e24a9ff0)/values'
        skip = (page*size) - size
        payload = {'$top': size, '$skip': skip}
        header = {'Content-Type': CONTENT_TYPE,'Authorization': f'Bearer {token}'}
        response_bizagi_projectsparticipant = requests.get(url_get_projectsparticipant, params=payload, headers=header)
        json_list_projectsparticipant = response_bizagi_projectsparticipant.json()

        if NEXT_LINK not in json_list_projectsparticipant:
            repeat = False

        for x in json_list_projectsparticipant['value']:
            parameters = x['parameters']
            for i in parameters:
                if (i['xpath'] == XPATH_CODE_PROJECT and i['value'] == id_project_jira):
                    for j in parameters:
                        if (j['xpath'] == 'ListadeParticipantes.sCodigoParticipante' and j['value'] == accound_id):
                            id_projectsparticipant = x['id']
                            return id_projectsparticipant
        page += 1

    return id_projectsparticipant

def post_projects_participants(list_participants_jira, id_project_bizagi, id_project_jira, token):
    users_add = []
    users_update = []
    users_register = {'id_project_jira': id_project_jira}
    # Obtener los participantes de la tabla intermedia proyectoParticipante de bizagi según el id de proyecto de jira
    bizagi_participants_by_project = get_list_participants_projects(token, id_project_jira)
    logger.info('ProyectoParticipante ->  {}: {}',id_project_jira, bizagi_participants_by_project)

    # Obtener los participantes de jira que no estén en la lista de ProyectoParticipante de bizagi, para agregarlo a la entidad ProyectoParticipante
    participants_not_in_bizagi = [item for item in list_participants_jira if item not in bizagi_participants_by_project]
    logger.info('Participantes registrados en Jira que no están en Bizagi para el proyecto "{}" - {}',id_project_jira, participants_not_in_bizagi)
    for i in participants_not_in_bizagi:
        # Buscar id de participante en Bizagi a partir de accoundId de participante de Jira
        id_participant_bizagi = get_iduser_participants(i, token)
        # validar que participante exista en la entidad listaParticipante de Bizagi
        if id_participant_bizagi != None:
            logger.info('Id de usuario de Jira encontrado en la entidad Participantes - "{}" ', i)
            url_project_participant = f'{server_bizagi}/Iniciativa/odata/data/entities(034eea50-607c-4289-b2e4-5933e24a9ff0)/create'
            header = {'Content-Type': CONTENT_TYPE,'Authorization': f'Bearer {token}'}
            payload = json.dumps({
                "startParameters": [
                    {
                        "xpath": "ListadeProyectos",
                        "value": id_project_bizagi
                    },
                    {
                        "xpath": "ListadeParticipantes",
                        "value": id_participant_bizagi
                    },
                    {
                        "xpath": "dsbl",
                        "value": "false"
                    }
                ]
            })
            response_create = requests.request("POST", url_project_participant, headers=header, data=payload)
            status_create = response_create.status_code
            if status_create == 200:
                users_add.append(i)
    # Adicionar lista de participantes creados para un proyecto
    users_register['project_paricipant_create'] = users_add
    # Obtener los participantes de bizagi que no estén en la lista de participantes de Jira, para proceder con su deshabilitación
    participants_not_in_jira = [item for item in bizagi_participants_by_project if item not in list_participants_jira]
    logger.info('Participantes registrados en Bizagi que no están en jira para el proyecto "{}" - {}',id_project_jira, participants_not_in_jira)
    
    for id_user_jira in participants_not_in_jira:
        id_projectsparticipant = get_id_projectsparticipant_by_accoundid_and_projectid(token, id_user_jira, id_project_jira)
        if id_projectsparticipant != None:
            response_dsbl = disable_participants_by_project(id_projectsparticipant, token)
            if response_dsbl == True:
                users_update.append(id_user_jira)
    # Adicionar lista de participantes actualizados(deshabilitar) para un proyecto
    users_register['project_paricipant_updated'] = users_update
    return users_register


def get_list_participants_projects(token, id_project_jira):
    participants = []
    size = 50
    page = 1
    repeat = True
    while repeat:
        # ListadeProyectos(9978487f-8368-4ace-98a5-55516e9df27c): Nombre de la entidad donde se guardan datos del los projectos de Jira
        url_idparticipant = f'{server_bizagi}/Iniciativa/odata/data/entities(034eea50-607c-4289-b2e4-5933e24a9ff0)/values'
        skip = (page*size) - size
        payload = {'$top': size, '$skip': skip}
        header = {'Content-Type': CONTENT_TYPE,'Authorization': f'Bearer {token}'}
        response_bizagi_participants = requests.get(url_idparticipant, params=payload, headers=header)
        json_list_participants = response_bizagi_participants.json()
        if NEXT_LINK not in json_list_participants:
            repeat = False

        for x in json_list_participants['value']:
            parameters = x['parameters']
            for i in parameters:
                if (i['xpath'] == XPATH_CODE_PROJECT and i['value'] == id_project_jira):
                    for j in parameters:
                        if j['xpath'] == XPATH_CODE_PARTICIPANT:
                            participants.append(j['value'])
        page += 1
    return participants


def get_list_projects_participants(bizagi_token):
    size = 50
    page = 1
    repeat = True
    list_bizagi_projects = []
    while repeat:
        # ListadeProyectos(9978487f-8368-4ace-98a5-55516e9df27c): Nombre de la entidad donde se guardan datos del los projectos de Jira
        url_list_projects_bizagi = f'{server_bizagi}/Iniciativa/odata/data/entities(034eea50-607c-4289-b2e4-5933e24a9ff0)/values'
        skip = (page*size) - size
        param = {'$top': size, '$skip': skip}
        response_bizagi_projects = requests.get(url_list_projects_bizagi, params=param, headers={'Content-type': CONTENT_TYPE, 'Authorization': f'Bearer {bizagi_token}'})
        response_json_list_projects = response_bizagi_projects.json()
        list_projec = response_json_list_projects['value']
        if NEXT_LINK not in response_json_list_projects:
            repeat = False

        for x in list_projec:
            parameters = x['parameters']
            projects_bizagi = {
                'code_participant': None,
                'code_project': None
            }
            for i in parameters:
                try:
                    if i['xpath'] == XPATH_CODE_PARTICIPANT:
                        projects_bizagi['code_participant'] = i['value']
                except Exception as e:
                    logger.info(
                        "No se encontro xpath sCodigoParticipante - {}", e)
                try:
                    if i['xpath'] == XPATH_CODE_PROJECT:
                        projects_bizagi['code_project'] = i['value']
                except Exception as e:
                    logger.info("No se encontro xpath sIddeproyecto - {}", e)

            list_bizagi_projects.append(projects_bizagi)
        page += 1

    return list_bizagi_projects


def get_participants_by_id_roles(url):
    list_actors = []
    header = {'Accept': CONTENT_TYPE,'Authorization': f'Basic {BasicAuth}'}
    response_team = requests.request('GET', url, headers=header)
    response_json = response_team.json()
    # Obtener lista de actores
    actors = response_json['actors']
    if actors:
        for i in actors:
            list_actors.append(i['actorUser']['accountId'])

    return list_actors


def get_team_participants_jira_for_id(id_project):
    team = []
    url_team = f'https://alignet.atlassian.net/rest/api/3/project/{id_project}/role'
    header = {'Accept': CONTENT_TYPE,'Authorization': f'Basic {BasicAuth}'}
    response_team = requests.request('GET', url_team, headers=header)
    response_json = response_team.json()
    logger.info('Roles para el proyecto - {}', response_json)
    link_roles = [v for k, v in response_json.items() if k in ['Team', 'Jefe de Proyecto', 'Scrum Master']]
    for l in link_roles:
        actors = get_participants_by_id_roles(l)
        for t in actors:
            if t not in team:
                team.append(t)
    return team


def handler(event, context):
    logger.info('Event - {}', json.dumps(event))
    # Variable
    msg_registers = []

    # Obtener Token de Bizagi
    response_bizagi_token = get_bizagi_token()
    if response_bizagi_token['statusCode'] != 200:
        logger.info(response_bizagi_token)
        return response_bizagi_token
    json_body = json.loads(response_bizagi_token['body'])
    token = json_body['access_token']

    # Obtener proyectos de la lista de proyectos de Bizagi
    list_projecs_bizagi = get_list_projects_bizagi(token)
    logger.info('Listado de proyectos de Bizagi - {}', list_projecs_bizagi)
    for i in list_projecs_bizagi:
        id_project_jira = i['id_jira']
        id_project_bizagi = i['id_bizagi']
        logger.info('Id Jira - {}', id_project_jira)
        logger.info('Id Bizagi - {}', id_project_bizagi)
        list_participants = get_team_participants_jira_for_id(id_project_jira)
        logger.info('Lista de Id de participantes de Jira - {}',list_participants)
        if list_participants:
            msg_rpta = post_projects_participants(list_participants, id_project_bizagi, id_project_jira, token)
            if msg_rpta:
                msg_registers.append(msg_rpta)
    # Validar si existen mensajes de registro exitoso de participantes por proyecto
    if not msg_registers:
        prepared_body = get_prepared_body('post_project_participants', 'No existen cambios entre los participantes de Jira y Bizagi para un proyecto registrado', True, data=msg_registers, code='00')
        return get_prepared_response(200, prepared_body)

    prepared_body = get_prepared_body('post_project_participants', 'Se regitraron Cambios entre los participantes para un proyecto registrado', True, data=msg_registers, code='00')
    return get_prepared_response(200, prepared_body)